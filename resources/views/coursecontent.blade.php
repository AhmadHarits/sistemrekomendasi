@extends('main')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table</h3>
              <br><button type="button" class="btn btn-primary form-new">NEW</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID Mata Kuliah</th>
                    <th>Nama Mata Kuliah</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($courses))
                @foreach($courses as $course)  
                <tr>
                    <td>{{$course->id_mk}}</td>
                    <td>{{$course->nama_mk}}</td>
                    <td><button type="button" class="btn btn-primary form-submit" data-key="{{$course->id_mk}}">edit</button>
                    <a href="{{URL::action('CourseController@delete', ['id'=>$course->id_mk])}}"><button type="button" class="btn btn-primary">delete</button></a></td>
                </tr>
                @endforeach
                @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <!-- MODAL INPUT -->
<div class="modal fade" id="modal-inputCourse" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form action="{{ URL::action('CourseController@new') }}" method="POST">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
        <h5>form input</h5>

          {{csrf_field()}}

            <div>
            <input type="text" name="nama_mk" required/><label>Nama Mata Kuliah</label>
            </div>
            
          <!-- <button type="submit">submit</button> -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
      </div>
	  </form>
  </div>
</div>

<!-- MODAL UPDATE -->
<div class="modal fade" id="modal-updateCourse" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="{{ URL::action('CourseController@update') }}" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
	        <h5>form update</h5>
        	{{csrf_field()}}
          <div>
            <input type="hidden" name="id_mk"/>
            <input type="text" name="nama_mk" required/><label>Nama Mata Kuliah</label>
            </div>
            
          
          <!-- <button type="submit">submit</button> -->
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
	    </div>
  	</form>
  </div>
</div>

  @endsection