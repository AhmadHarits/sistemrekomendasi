@extends('main')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table</h3>
              <br><button type="button" class="btn btn-primary form-new">NEW</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID Akun</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>NIM / NIP / NIDK</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($accounts))
                @foreach($accounts as $account)  
                <tr>
                    <td>{{$account->id_akun}}</td>
                    <td>{{$account->username}}</td>
                    <td>{{$account->password}}</td>
                    <td>{{$account->id_pemilik}}</td>
                    <td><button type="button" class="btn btn-primary form-submit-akun" data-key="{{$account->id_akun}}">edit</button>
                    <a href="{{URL::action('AccountController@delete', ['id'=>$account->id_akun])}}"><button type="button" class="btn btn-primary">delete</button></a></td>
                </tr>
                @endforeach
                @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <!-- MODAL INPUT -->
<div class="modal fade" id="modal-inputAccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form action="{{ URL::action('AccountController@new') }}" method="POST">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
        <h5>form input</h5>

          {{csrf_field()}}

            <div>
            <input type="text" name="username" required/><label>Username</label>
            </div>
            <div>
            <input type="text" name="password" required/><label>Password</label>
            </div>
            <div>
            <input type="text" name="nim_nip_nidk" required/><label>NIM/NIP/NIDK</label>
            </div>
            <div>
            <input type="text" name="nama" required/><label>Nama</label>
            </div>
            
          <!-- <button type="submit">submit</button> -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
      </div>
	  </form>
  </div>
</div>

<!-- MODAL UPDATE -->
<div class="modal fade" id="modal-updateAccount" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="{{ URL::action('AccountController@update') }}" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
	        <h5>form update</h5>
        	{{csrf_field()}}
          <div>
            <input type="hidden" name="id_akun"/>
            <input type="text" name="nama_mk" required/><label>Nama Mata Kuliah</label>
            </div>
            
          
          <!-- <button type="submit">submit</button> -->
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
	    </div>
  	</form>
  </div>
</div>

  @endsection