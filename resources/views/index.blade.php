<!DOCTYPE html>
<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

   <!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
	<title>Sis.Rek.PTIK</title>
	<meta name="description" content="">  
	<meta name="author" content="">

   <!-- mobile specific metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 	<!-- CSS
   ================================================== -->
   <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{URL::asset('/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
   <link rel="stylesheet" href="{{URL::asset('/css/base.css')}}">
   <link rel="stylesheet" href="{{URL::asset('/css/vendor.css')}}">  
   <link rel="stylesheet" href="{{URL::asset('/css/main.css')}}">  

   <!-- script
   ================================================== -->
	<script src="{{URL::asset('/js/index/modernizr.js')}}"></script>
	<script src="{{URL::asset('/js/index/pace.min.js')}}"></script>

   <!-- favicons
	================================================== -->
	<link rel="shortcut icon" href="{{URL::asset('/favicon.ico')}}" type="image/x-icon">
	<link rel="icon" href="{{URL::asset('/favicon.ico')}}" type="image/x-icon">

</head>

<body id="top">

	<!-- header 
   ================================================== -->
   <header> 

   	<div class="header-logo">
	      <a href="/">SISTEM REKOMENDASI PTIK</a>
	   </div> 

		<a id="header-menu-trigger" href="#0">
		 	<span class="header-menu-text">Menu</span>
		  	<span class="header-menu-icon"></span>
		</a> 

		<nav id="menu-nav-wrap">

			<a href="#0" class="close-button" title="close"><span>Close</span></a>	

	   	<h3>SISTEM REKOMENDASI PTIK</h3>  

			<ul class="nav-list">
				<li class="current"><a class="smoothscroll" href="#home" title="">Halaman Utama</a></li>
				<li><a class="smoothscroll" href="#about" title="">Tentang Kami</a></li>
				<li><a class="smoothscroll" href="#services" title="">PTIK</a></li>
					
			</ul>			

		</nav>  <!-- end #menu-nav-wrap -->

	</header> <!-- end header -->  


   <!-- home
   ================================================== -->
   <section id="home">

   	<div class="overlay"></div>

   	<div class="home-content-table">	
		   <div class="home-content-tablecell">
		   	<div class="row">
		   		<div class="col-twelve">		   			
							
							@if(\Session::get('error'))
							<div class="more animate-intro">
								<!-- <p class="alert alert-danger"> -->
								<a class="button stroke">
								{{\Session::get('error')}}
								</a>
							<!-- </p> -->
							</div>
							@endif
			  		
				  			<h1 class="animate-intro">
							SISTEM<br>
                            REKOMENDASI<br>
							PTIK
				  			</h1>	
				  			<div class="more animate-intro">
				  				<a class="button stroke form-login" >
				  					lOGIN
				  				</a>
				  			</div>							

			  		</div> <!-- end col-twelve --> 
		   	</div> <!-- end row --> 
		   </div> <!-- end home-content-tablecell --> 		   
		</div> <!-- end home-content-table -->	

		<div class="scrolldown">
			<a href="#about" class="scroll-icon smoothscroll">		
		   	Scroll Down		   	
		   	<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
			</a>
		</div>			
   
   </section> <!-- end home -->


   <!-- about
   ================================================== -->
   <section id="about">

   	<div class="row about-wrap">
   		<div class="col-full">

   			<div class="about-profile-bg"></div>

				<div class="intro">
					<h3 class="animate-this">Tentang Kami</h3>
	   			<p class="lead animate-this"><span>Sistem Rekomendasi PTIK</span> merupakan sebuah sistem yang dibangun untuk mendukung mahasiswa dalam melakukan pengambilan keputusan ketika akan menentukan peminatan pada semester 5.</p>	
				</div>   


   		</div> <!-- end col-full  -->
   	</div> <!-- end about-wrap  -->

   </section> <!-- end about -->


   <!-- about
   ================================================== -->
   <section id="services">

   	<div class="overlay"></div>
   	<div class="gradient-overlay"></div>
   	
   	<div class="row narrow section-intro with-bottom-sep animate-this">
   		<div class="col-full">
   			
   				<h3>Peminatan</h3>
   			   <h1>P.T.I.K</h1>
   			
   			   <p class="lead">Pendidikan Teknik Informatika dan Komputer disingkat PTIK merupakan salah satu program studi di Fakultas Teknik Universitas Negeri Jakarta. Program studi ini memiliki 3 peminatan yang akan dipilih pada semester 5. Setiap peminatan memiliki kebutuhan skill dan prospek kerja yang berbeda-beda</p>
   			
   	   </div> <!-- end col-full -->
   	</div> <!-- end row -->

   	<div class="row services-content">

   		<div class="services-list block-5-1 block-tab-full group">

			  	@if(isset($specializations))
              	@foreach($specializations as $specialization)  

				<div class="bgrid service-item animate-this">

					<span class="icon"><i class="fa fa-{{$specialization->simbol}}"></i></span>	              

	            <div class="service-content">
	            	<h3 class="h05">{{$specialization->nama_peminatan}}</h3>
					<p><strong>DESKRIPSI</strong><p>
		            <p>{{$specialization->deskripsi}}
					<p><strong>KEBUTUHAN SKILL</strong><p>
		            <p>{{$specialization->kebutuhan_skill}}
					<p><strong>PROSPEK KERJA<strong><p>
		            <p>{{$specialization->prospek_kerja}}
						
					</p>	         		
	            </div>                

				</div> <!-- end bgrid -->			   
				@endforeach
				@endif
	      </div> <!-- end services-list -->
   		
   	</div> <!-- end services-content -->   			

   </section> <!-- end services -->


   <!-- portfolio
   ================================================== -->
   <!-- end portfolio -->





	<!-- stats
   ================================================== -->
   


	<!-- contact
   ================================================== -->
  


	<!-- footer
   ================================================== -->
	

   	<div class="footer-bottom">

      	<div class="row">

      		<div class="col-twelve">
	      		<div class="copyright">
		         	<span>© Copyright PTIK 2019</span> 
		         	<span>Design by <a href="">PTIK UNJ</a></span>		         	
		         </div>		               
	      	</div>

      	</div>   	

      </div> <!-- end footer-bottom -->

      <div id="go-top">
		   <a class="smoothscroll" title="Back to Top" href="#top">
		   	<i class="fa fa-long-arrow-up" aria-hidden="true"></i>
		   </a>
		</div>		
   </footer>

   <div id="preloader"> 
    	<div id="loader"></div>
   </div> 


<!-- MODAL INPUT -->
<div class="modal fade" id="modal-login" tabindex="" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form action="{{ URL::action('AccountController@login') }}" method="POST">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          <div class="modal-header">
          <center><h1 class="modal-title" id="exampleModalLabel">SISTEM REKOMENDASI PTIK</h1>
			</center>
          </div>
          <div class="modal-body">
        <!-- <h5>form input</h5> -->

          {{csrf_field()}}

<div class="form-group">
    <label for="exampleInputEmail1">Username</label>
    <input name="username" required type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Help" placeholder="Enter username">
    <!-- <small id="Help" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input name="password" required type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  
  <button type="submit" class="btn btn-primary">Login</button>

            <!-- <div>
            <center><label>USERNAME</label><input placeholder="username" type="text" name="username" required/></center>
            </div>
            <div>
			<center><label>PASSWORD</label><input placeholder="password" type="password" name="password" required/></center>
            </div> -->
            
          <!-- <button type="submit">submit</button> -->
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn btn-info pull-right">LOGIN</button>
          </div> -->
      </div>
	  </form>
  </div>
</div>



   <!-- Java Script
   ================================================== --> 

   <!-- Bootstrap 3.3.7 -->
   <script src="{{URL::asset('/js/index/jquery-2.1.3.min.js')}}"></script>
<script src="{{URL::asset('/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

   <script src="{{URL::asset('/js/index/plugins.js')}}"></script>
   <script src="{{URL::asset('/js/index/main.js')}}"></script>
   <script src="{{URL::asset('/js/login.js')}}"></script>

</body>

</html>