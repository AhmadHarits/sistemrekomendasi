<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Simple Tables</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.4.1/css/ionicons.min.css">
  <!-- Data Tables -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.8/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.8/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body>
<td><button type="button" class="btn btn-primary form-new">NEW</button></td>
             <table>
             <thead>
              <tr>
                <th>NIM/NIP/NIDK</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>E-mail</th>
                <th>No Telp</th>
                <th>Role</th>
                <th>Peminatan</th>
                <th>Aksi</th>
              </tr>
              </thead>
              <tbody>
              @if(isset($profiles))
              @foreach($profiles as $profile)  
              <tr>
                <td>{{$profile->nim_nip_nidk}}</td>
                <td>{{$profile->nama}}</td>
                <td>{{$profile->jenis_kelamin}}</td>
                <td>{{$profile->e_mail}}</td>
                <td>{{$profile->no_telp}}</td>
                <td>{{$profile->role}}</td>
                <td>{{$profile->peminatan}}</td>
                <td><button type="button" class="btn btn-primary form-submit" data-key="{{$profile->nim_nip_nidk}}">edit</button></td>
                <td> <a href="{{URL::action('ProfileController@delete', ['id'=>$profile->id])}}"><button type="button" class="btn btn-primary">delete</button></a></td>
              </tr>
              @endforeach
              @endif
              </tbody>
            </table>

<!-- MODAL INPUT -->
<div class="modal fade" id="modal-input" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form action="{{ URL::action('ProfileController@new') }}" method="POST">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
        <h5>form input</h5>

          {{csrf_field()}}

            <div>
            <input type="text" name="nim_nip_nidk" required/><label>nim_nip_nidk</label>
            </div>
            <div>
            <input type="text" name="nama" required/><label>nama</label>
            </div>
            <div>
            <input type="text" name="jenis_kelamin" required/><label>jenis_kelamin</label>
            </div>
            <div>
            <input type="text" name="e_mail" required/><label>e_mail</label>
            </div>
            <div>
            <input type="text" name="no_telp" required/><label>no_telp</label>
            </div>
            <div>
            <input type="text" name="role"/><label>role</label>
            </div>
            <div>
            <input type="text" name="peminatan"/><label>peminatan</label>
          <!-- <button type="submit">submit</button> -->
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
      </div>
	  </form>
  </div>
</div>

<!-- MODAL UPDATE -->
<div class="modal fade" id="modal-update" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="{{ URL::action('ProfileController@update') }}" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
	        <h5>form update</h5>
        	{{csrf_field()}}
          <div>
            <input type="text" name="nim_nip_nidk" required/><label>nim_nip_nidk</label>
          </div>
          <div>
            <input type="text" name="nama" required/><label>nama</label>
            </div>
          <div>
            <input type="text" name="jenis_kelamin" required/><label>jenis_kelamin</label>
            </div>
          <div>
            <input type="text" name="e_mail" required/><label>e_mail</label>
          </div>
          <div>
            <input type="text" name="no_telp" required/><label>no_telp</label>
          </div>
          <div>
            <input type="text" name="role"  /><label>role</label>
          </div>
          <div>
            <input type="text" name="peminatan" /><label>peminatan</label>
          <!-- <button type="submit">submit</button> -->
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
	    </div>
  	</form>
  </div>
</div>


<!-- jQuery 3 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- popper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.8/js/adminlte.min.js"></script>
<!-- editprofile -->
<script src="{{URL::asset('/js/editProfile.js')}}"></script>
<!-- newprofile -->
<script src="{{URL::asset('/js/newProfile.js')}}"></script>

</body>
</html>