@extends('main')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID Akun</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>NIM / NIP / NIDK</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($accounts))
                @foreach($accounts as $account)  
                <tr>
                    <td>{{$account->id_akun}}</td>
                    <td>{{$account->username}}</td>
                    <td>{{$account->password}}</td>
                    <td>{{$account->id_pemilik}}</td>
                    <td><button type="button" class="btn btn-primary form-lihat-profil" data-key="{{$account->id_pemilik}}" data-key2="{{\Session::get('userid')}}">Lihat Profil</button></td>
                </tr>
                @endforeach
                @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>Rendering engine</th>
                  <th>Browser</th>
                  <th>Platform(s)</th>
                  <th>Engine version</th>
                  <th>CSS grade</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<!-- MODAL UPDATE -->
<div class="modal fade" id="modal-lihat-profil" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="{{ URL::action('AccountController@update') }}" method="POST">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-header">
          <center><h1 class="modal-title" id="exampleModalLabel">DATA DIRI</h1></center>
        </div>
        <div class="modal-body">
            <divisi><h3>
              <table>
               <tbody>
                <tr>  
              <td>NIM/NIP/NIDK</td> 
              <td>: <span-nim_nip_nidk></span-nim_nip_nidk></td>
                </tr>
                <tr>  
              <td>NAMA </td> 
              <td>: <span-nama></span-nama></td>
                </tr>
                <tr>  
              <td>JENIS KELAMIN&emsp;</td> 
              <td>: <span-jenis_kelamin></span-jenis_kelamin></td>
                </tr>
                <tr>  
              <td>E-MAIL</td> 
              <td>: <span-e_mail></span-e_mail></td>
                </tr>
                <tr>  
              <td>NO. TELEPON</td> 
              <td>: <span-no_telp></span-no_telp></td>
                </tr>
                <tr>  
              <td>ROLE</td> 
              <td>: <span-role></span-role></td>
                </tr>
                <tr>  
              <td>PEMINATAN</td> 
              <td>: <span-peminatan></span-peminatan></td>
                </tr>
                </tbody>
              </table>
            </h3></divisi>

            <!-- <mydiv id="mydiv" data-myval="JohnCena"></mydiv> -->
            
          
          <!-- <button type="submit">submit</button> -->
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <!-- <button type="submit" class="btn btn-primary">Save changes</button> -->
        </div>
	    </div>
  	</form>
  </div>
</div>

@endsection