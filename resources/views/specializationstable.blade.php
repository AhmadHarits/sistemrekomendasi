  <!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Simple Tables</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.4.1/css/ionicons.min.css">
  <!-- Data Tables -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.8/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.8/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body>
<td><button type="button" class="btn btn-primary form-new">NEW</button></td>
             <table>
             <thead>
              <tr>
                <th>ID Peminatan</th>
                <th>Nama Peminatan</th>
                <th>Simbol</th>
                <th>Deskripsi</th>
                <th>Kebutuhan Skill</th>
                <th>Prospek Kerja</th>
                <th>Nip / Nidk</th>
                <th>Aksi</th>
              </tr>
              </thead>
              <tbody>
              @if(isset($specializations))
              @foreach($specializations as $specialization)  
              <tr>
                <td>{{$specialization->id_peminatan}}</td>
                <td>{{$specialization->nama_peminatan}}</td>
                <td>{{$specialization->simbol}}</td>
                <td>{{$specialization->deskripsi}}</td>
                <td>{{$specialization->kebutuhan_skill}}</td>
                <td>{{$specialization->prospek_kerja}}</td>
                <td>{{$specialization->nip_nidk}}</td>
                <td><button type="button" class="btn btn-primary form-submit" data-key="{{$specialization->id_peminatan}}">edit</button></td>
                <td> <a href="{{URL::action('SpecializationController@delete', ['id'=>$specialization->id_peminatan])}}"><button type="button" class="btn btn-primary">delete</button></a></td>
              </tr>
              @endforeach
              @endif
              </tbody>
            </table>

<!-- MODAL INPUT -->
<div class="modal fade" id="modal-input" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form action="{{ URL::action('SpecializationController@new') }}" method="POST">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
        <h5>form input</h5>

          {{csrf_field()}}
          
            <div>
            <input type="text" name="nama_peminatan" required/><label>Nama Peminatan</label>
            </div>
            <div>
            <input type="text" name="simbol" required/><label>Simbol</label>
            </div>
            <div>
            <input type="text" name="deskripsi" required/><label>Deskripsi</label>
            </div>
            <div>
            <input type="text" name="kebutuhan_skill" required/><label>Kebutuhan Skill</label>
            </div>
            <div>
            <input type="text" name="prospek_kerja" required/><label>Prospek Kerja</label>
            </div>
            <div>
            <input type="text" name="nip_nidk" required/><label>NIP / NIDK</label>
            </div>
          <!-- <button type="submit">submit</button> -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
      </div>
	  </form>
  </div>
</div>

<!-- MODAL UPDATE -->
<div class="modal fade" id="modal-update" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="{{ URL::action('SpecializationController@update') }}" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
	        <h5>form update</h5>
        	{{csrf_field()}}
            <div>
            <input type="hidden" name="id_peminatan"/>
            </div>
            <div>
            <input type="text" name="nama_peminatan" required/><label>Nama Peminatan</label>
            </div>
            <div>
            <input type="text" name="simbol" required/><label>Simbol</label>
            </div>
            <div>
            <input type="text" name="deskripsi" required/><label>Deskripsi</label>
            </div>
            <div>
            <input type="text" name="kebutuhan_skill" required/><label>Kebutuhan Skill</label>
            </div>
            <div>
            <input type="text" name="prospek_kerja" required/><label>Prospek Kerja</label>
            </div>
            <div>
            <input type="text" name="nip_nidk" required/><label>NIP / NIDK</label>
            </div>
          
          <!-- <button type="submit">submit</button> -->
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
	    </div>
  	</form>
  </div>
</div>


<!-- jQuery 3 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- popper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.8/js/adminlte.min.js"></script>
<!-- editspecialization -->
<script src="{{URL::asset('/js/editSpecialization.js')}}"></script>
<!-- newSpecialization -->
<script src="{{URL::asset('/js/newSpecialization.js')}}"></script>

</body>
</html>