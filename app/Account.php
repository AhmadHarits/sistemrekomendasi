<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    protected $fillable = ['username','password','id_pemilik'];
    protected $primaryKey = 'id_akun';
}
