<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $fillable = ['nim_nip_nidk','nama','jenis_kelamin','e_mail','no_telp','role','peminatan'];

    
    // protected $primaryKey = 'nim_nip_nidk';
}
