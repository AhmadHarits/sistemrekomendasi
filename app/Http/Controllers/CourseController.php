<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;

class CourseController extends Controller
{
    //
    public function index(){
        $courses = Course::all();
        return view('coursecontent', ['courses'=>$courses]);
   }
   public function getById($id_mk){
     // dd($nim_nip_nidk);
     $course = Course::find($id_mk);
    //  dd($course);
    return response()->json([
      'data'=>$course
      ]);
      dd($course);
   }

   public function new(Request $req){
       
     $data = new Course();
     $data->nama_mk=$req->input('nama_mk');
     $data->save();
     return redirect()->action('CourseController@index');
 }

   public function update(Request $req){
    //  dd($req->input('deskripsi'));
     $course = Course::where('id_mk', $req->input('id_mk'))->first();
    //  dd($course);
     // dd($req->input('id'));
     $course->nama_mk=$req->input('nama_mk');
     $course->save();
     return redirect()->action('CourseController@index');
     
   }

   public function delete($id_mk){
     $course = Course::find($id_mk);
     $course->delete();
     return redirect()->action('CourseController@index');
   }
}
