<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;


class ProfileController extends Controller
{
    //
    public function index(){
		 $profiles = Profile::all();
		 return view('profilestable', ['profiles'=>$profiles]);
    }
    public function getbyid($nim_nip_nidk){
      // dd($nim_nip_nidk);
      $profile = Profile::where('nim_nip_nidk', $nim_nip_nidk)->first();
      
      return response()->json([
        'data'=>$profile
        ]);
    }

    public function new(Request $req){
        
      $data = new Profile();
      $data->nim_nip_nidk=$req->input('nim_nip_nidk');
      $data->nama=$req->input('nama');
      $data->jenis_kelamin=$req->input('jenis_kelamin');
      $data->e_mail=$req->input('e_mail');
      $data->no_telp=$req->input('no_telp');
      $data->role=$req->input('role');
      $data->peminatan=$req->input('peminatan');
      $data->save();
      return redirect()->action('ProfileController@index');
  }

    public function update(Request $req){
      $profile = Profile::where('nim_nip_nidk', $req->input('nim_nip_nidk'))->first();
      // dd($req->input('id'));
      $profile->nama=$req->input('nama');
      $profile->jenis_kelamin=$req->input('jenis_kelamin');
      $profile->e_mail=$req->input('e_mail');
      $profile->no_telp=$req->input('no_telp');
      $profile->role=$req->input('role');
      $profile->peminatan=$req->input('peminatan');
      $profile->save();
      return redirect()->action('ProfileController@index');
      
    }

    public function delete($id){
      $profile = Profile::find($id);
  
      $profile->delete();
      return redirect()->action('ProfileController@index');
    }

    
}
