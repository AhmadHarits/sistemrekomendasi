<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Specialization;

class SpecializationController extends Controller
{
    //
    public function index(){
        $specializations = Specialization::all();
        return view('specializationstable', ['specializations'=>$specializations]);
   }
   public function getById($id_peminatan){
     // dd($nim_nip_nidk);
     $specialization = Specialization::find($id_peminatan);
    //  dd($specialization);
    return response()->json([
      'data'=>$specialization
      ]);
      dd($specialization);
   }

   public function new(Request $req){
       
     $data = new Specialization();
     $data->nama_peminatan=$req->input('nama_peminatan');
     $data->simbol=$req->input('simbol');
     $data->deskripsi=$req->input('deskripsi');
     $data->kebutuhan_skill=$req->input('kebutuhan_skill');
     $data->prospek_kerja=$req->input('prospek_kerja');
     $data->nip_nidk=$req->input('nip_nidk');
     $data->save();
     return redirect()->action('SpecializationController@index');
 }

   public function update(Request $req){
    //  dd($req->input('deskripsi'));
     $specialization = Specialization::where('id_peminatan', $req->input('id_peminatan'))->first();
    //  dd($specialization);
     // dd($req->input('id'));
     $specialization->nama_peminatan=$req->input('nama_peminatan');
     $specialization->simbol=$req->input('simbol');
     $specialization->deskripsi=$req->input('deskripsi');
     $specialization->kebutuhan_skill=$req->input('kebutuhan_skill');
     $specialization->prospek_kerja=$req->input('prospek_kerja');
     $specialization->nip_nidk=$req->input('nip_nidk');
     $specialization->save();
     return redirect()->action('SpecializationController@index');
     
   }

   public function delete($id_peminatan){
     $specialization = Specialization::find($id_peminatan);
 
     $specialization->delete();
     return redirect()->action('SpecializationController@index');
   }

   public function home(){
    $specializations = Specialization::all();
    return view('index', ['specializations'=>$specializations]);
   }
}
