<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Profile;


class AccountController extends Controller
{
    //
    public function login(Request $req){
        $account = Account::where('username', $req->input('username'))->first();
        if($account != null){
            \Session::put('userid',$account->id_pemilik);
            $profile = Profile::where('nim_nip_nidk', $account->id_pemilik)->first();
            if($profile->role == 'admin'){           
            return redirect('home/admin');
                }
            if($profile->role == 'dosen'){           
                return redirect('home/dosen');
                }
            if($profile->role == 'mahasiswa'){           
               return redirect('home/mahasiswa');
                }
            return redirect('');

        }else{
            \Session::flash('error', 'USERNAME TIDAK DITEMUKAN');
            return redirect('');
        }
    }
    public function logout(){
        \Session::put('userid', null);
        return redirect('');
    }
    public function home(){
        $accounts = Account::all();
        return view('admincontent',['accounts'=>$accounts]);
    }
    public function index(){
        $accounts = Account::all();
        // dd($nim_nip_nidk);
        return view('accountcontent', ['accounts'=>$accounts]);
   }
   public function getById($id_akun){
     // dd($nim_nip_nidk);
     $account = Account::find($id_akun);
    //  dd($account);
    return response()->json([
      'data'=>$account
      ]);
      dd($account);
   }

   public function new(Request $req){
       
        $data2 = new Profile();
        $data2->nim_nip_nidk=$req->input('nim_nip_nidk');
        $data2->nama=$req->input('nama');
        $data2->save();
        $data = new Account();
        $data->username=$req->input('username');
        $data->password=$req->input('password');
        $data->id_pemilik=$req->input('nim_nip_nidk');
        $data->save();
     return redirect()->action('AccountController@index');
 }

   public function update(Request $req){
    //  dd($req->input('deskripsi'));
     $account = Account::where('id_akun', $req->input('id_akun'))->first();
    //  dd($account);
     // dd($req->input('id'));
     $account->username=$req->input('username');
     $account->password=$req->input('password');
     $account->id_pemilik=$req->input('id_pemilik');
     $account->save();
     return redirect()->action('AccountController@index');
     
   }

   public function delete($id_akun){
     $account = Account::find($id_akun);
     $account->delete();
     return redirect()->action('AccountController@index');
   }
}
