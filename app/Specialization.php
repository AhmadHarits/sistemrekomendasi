<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    protected $fillable = ['nama_peminatan','simbol','deskripsi','kebutuhan_skill','prospek_kerja','nip_nidk'];
    protected $primaryKey = 'id_peminatan';
    //
}
