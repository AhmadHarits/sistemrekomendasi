<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //
    protected $fillable = ['nama_mk'];
    protected $primaryKey = 'id_mk';
}
