<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/test', function () {
//     return view('content');
// });

Route::get('/', 'SpecializationController@home');
Route::post('/login', 'AccountController@login');

Route::post('/profile/new', 'ProfileController@new');
Route::get('/profile/{nim_nip_nidk}','ProfileController@getbyid');
Route::post('/profile/update', 'ProfileController@update');
Route::get('/profile/delete/{id}', 'ProfileController@delete');

Route::post('/specialization/new', 'SpecializationController@new');
Route::get('/specialization/{id_peminatan}','SpecializationController@getById');
Route::post('/specialization/update', 'SpecializationController@update');
Route::get('/specialization/delete/{id_peminatan}', 'SpecializationController@delete');

Route::post('/course/new', 'CourseController@new');
Route::get('/course/{id_mk}','CourseController@getById');
Route::post('/course/update', 'CourseController@update');
Route::get('/course/delete/{id_mk}', 'CourseController@delete');

Route::post('/account/new', 'AccountController@new');
Route::get('/account/{id_mk}','AccountController@getById');
Route::post('/account/update', 'AccountController@update');
Route::get('/account/delete/{id_mk}', 'AccountController@delete');


// Route::group(['middleware'=>['login']],function (){
    
    Route::get('/profile', function () { return view('profilestable'); });
    Route::get('/profile/all', 'ProfileController@index');
    
    
    Route::get('/specialization', function () { return view('specializationstable'); });
    Route::get('/specialization/all', 'SpecializationController@index');
    
    
    Route::get('/course', function () { return view('coursestable'); });
    Route::get('/course/all', 'CourseController@index');
    
    
    Route::get('/account', function () { return view('accountstable'); });
    Route::get('/home/admin', 'AccountController@home');
    Route::get('/account/all', 'AccountController@index');
    
    
// });