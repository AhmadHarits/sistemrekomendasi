-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Jan 2019 pada 01.13
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sisrekptik`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `accounts`
--

CREATE TABLE `accounts` (
  `id_akun` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `id_pemilik` varchar(18) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `accounts`
--

INSERT INTO `accounts` (`id_akun`, `username`, `password`, `id_pemilik`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', '5235154090', '2019-01-16 12:37:16', '2019-01-16 12:37:16'),
(2, 'dosen', 'dosen', '5235151282', '2019-01-17 10:05:03', '2019-01-17 10:05:03'),
(3, 'mahasiswa', 'mahasiswa', '5235151010', '2019-01-17 10:06:39', '2019-01-17 10:06:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `courses`
--

CREATE TABLE `courses` (
  `id_mk` int(11) NOT NULL,
  `nama_mk` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `courses`
--

INSERT INTO `courses` (`id_mk`, `nama_mk`, `created_at`, `updated_at`) VALUES
(2, 'Konsep Pemrogramana', '2019-01-16 05:31:30', '2019-01-16 08:32:17'),
(3, 'Algoritma Pemrograman 2', '2019-01-16 08:33:43', '2019-01-16 08:33:43'),
(4, 'as', '2019-01-16 09:14:59', '2019-01-16 09:14:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `paths`
--

CREATE TABLE `paths` (
  `id_path` int(11) NOT NULL,
  `role` varchar(50) NOT NULL,
  `path` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `paths`
--

INSERT INTO `paths` (`id_path`, `role`, `path`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'account/all', '2019-01-18 00:00:00', '2019-01-18 00:00:00'),
(2, 'admin', 'course/all', '2019-01-18 00:00:00', '2019-01-18 00:00:00'),
(3, 'admin', 'home/admin', '2019-01-20 00:00:00', '2019-01-20 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `nim_nip_nidk` varchar(18) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(25) DEFAULT NULL,
  `e_mail` varchar(50) DEFAULT NULL,
  `no_telp` varchar(13) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `peminatan` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profiles`
--

INSERT INTO `profiles` (`id`, `nim_nip_nidk`, `nama`, `jenis_kelamin`, `e_mail`, `no_telp`, `role`, `peminatan`, `created_at`, `updated_at`) VALUES
(1, '5235154090', 'Ahmad Harits Bachtiar Awalin', 'Pria', 'ahmad.harits1@gmail.com', '081298500866', 'admin', NULL, '2019-01-13 00:00:00', '2019-01-15 02:38:15'),
(2, '5235151282', 'Dewi Oktaviani', NULL, NULL, NULL, NULL, NULL, '2019-01-17 10:05:03', '2019-01-17 10:05:03'),
(3, '5235151010', 'Egy', NULL, NULL, NULL, NULL, NULL, '2019-01-17 10:06:39', '2019-01-17 10:06:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `specializations`
--

CREATE TABLE `specializations` (
  `id_peminatan` int(11) NOT NULL,
  `nama_peminatan` varchar(100) NOT NULL,
  `simbol` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `kebutuhan_skill` text NOT NULL,
  `prospek_kerja` text NOT NULL,
  `nip_nidk` varchar(18) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `specializations`
--

INSERT INTO `specializations` (`id_peminatan`, `nama_peminatan`, `simbol`, `deskripsi`, `kebutuhan_skill`, `prospek_kerja`, `nip_nidk`, `created_at`, `updated_at`) VALUES
(2, 'REKAYASA PERANGKAT LUNAK', 'code', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.', 'ASDF', 'ASDF', '5235154090', '2019-01-15 16:32:00', '2019-01-16 06:06:48'),
(3, 'TEKNIK JARINGAN DAN KOMPUTER', 'signal', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.', 'ASDFS', 'asdfasdf', '5235154090', '2019-01-15 16:32:14', '2019-01-16 06:06:56'),
(5, 'MULTIMEDIA', 'film', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.', 'asdfaaa', 'a', '5235154090', '2019-01-16 05:02:10', '2019-01-16 06:07:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suggestions`
--

CREATE TABLE `suggestions` (
  `id_saran` int(11) NOT NULL,
  `nim` varchar(18) NOT NULL,
  `nip_nidk` varchar(18) NOT NULL,
  `saran` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `values`
--

CREATE TABLE `values` (
  `id_nilai` int(11) NOT NULL,
  `nim` int(18) NOT NULL,
  `nama_mk` varchar(50) NOT NULL,
  `nilai` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id_akun`),
  ADD KEY `id_pemilik` (`id_pemilik`);

--
-- Indeks untuk tabel `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id_mk`);

--
-- Indeks untuk tabel `paths`
--
ALTER TABLE `paths`
  ADD PRIMARY KEY (`id_path`);

--
-- Indeks untuk tabel `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `specializations`
--
ALTER TABLE `specializations`
  ADD PRIMARY KEY (`id_peminatan`),
  ADD KEY `nip_nidk` (`nip_nidk`);

--
-- Indeks untuk tabel `suggestions`
--
ALTER TABLE `suggestions`
  ADD PRIMARY KEY (`id_saran`);

--
-- Indeks untuk tabel `values`
--
ALTER TABLE `values`
  ADD PRIMARY KEY (`id_nilai`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id_akun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `courses`
--
ALTER TABLE `courses`
  MODIFY `id_mk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `paths`
--
ALTER TABLE `paths`
  MODIFY `id_path` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `specializations`
--
ALTER TABLE `specializations`
  MODIFY `id_peminatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `suggestions`
--
ALTER TABLE `suggestions`
  MODIFY `id_saran` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `values`
--
ALTER TABLE `values`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
